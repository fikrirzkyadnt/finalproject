<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function create()
    {
        return view('layouts.posts.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pengaduan',
            'isi' => 'required',
            'kategori' => 'required'
        ]);
        
        //metode mass//
        $post = Post::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "kategori" => $request["kategori"],
            "masyarakat_id" => Auth::id()
        ]);
        return redirect('/home')->with('success', 'Pengaduan Berhasil Dibuat');
    }

    public function index(){
        // $user = Auth::user();
        // $posts = $user->posts; -> jika ingin menampilkan hanya berdasarkan user
        $posts = Post::all();

        return view('layouts.posts.index', compact('posts'));
    }

    public function show($id){
        //$show = DB::table('film')->where('id', $id)->first(); 
        // dd($show);
        $show = Post::find($id);
        return view('layouts.posts.show', compact('show'));  
    }

    

    public function edit($id)
    {
        //$post = DB::table('film')->where('id', $id)->first(); 
        $post = Post::find($id);
        return view('layouts.posts.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
    //     $request->validate([
    //         'judul' => 'required|unique:pengaduan',
    //         'desc' => 'required',
    //         'tahun' => 'required'
    //     ]);

        $update = Post::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "kategori" => $request["kategori"],
            "masyarakat_id" => Auth::id()
        ]);
        return redirect('/home')->with('success', 'Data pengaduan berhasil diperbaharui');
    }

    public function destroy($id)
    {
        // $query = DB::table('film')
        //             ->where('id', $id)
        //             ->delete();
        Post::destroy($id);
        return redirect('/home')->with('success', 'Data pengaduan berhasil dihapus');
    }
}
