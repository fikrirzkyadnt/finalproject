<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function createComment($id)
    {
        $show = Post::find($id);
        return view('layouts.posts.createKomentar', compact('show'));
    }

    public function comment($id, Request $request)
    {
        // dd($request->all());
        $request->validate([
            'isi' => 'required',
        ]);
        
        $show = Post::find($id);

        //metode mass//
        $post = Comment::create([
            "isi" => $request["isi"],
            "pengaduan_id" => $request["id"]
        ]);

        return redirect('/pengaduan/{id}')->with('success', 'Komentar Berhasil Dibuat');
    }

    public function show($id){
        
        // dd($show);
        $show = Post::find($id);
        $comments = Comment::all();
        
        // dd($comment);
        return view('layouts.posts.show', compact('comments', 'show'));  
    }
}
