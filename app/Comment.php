<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "komentar_pengaduan";

    protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User', 'masyarakat_id');
    }
}
