<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "pengaduan";

    //protected $fillable = ["judul", "isi"];

    protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User', 'masyarakat_id');
    }
    
}
