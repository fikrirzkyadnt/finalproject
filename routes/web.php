<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/', 'PostController@index');
Route::get('/home', 'PostController@index');

Route::get('/pengaduan/create', 'PostController@create');
Route::post('/pengaduan/create', 'PostController@store')->name('store');

Route::get('/pengaduan/{id}', 'PostController@show');
Route::get('/pengaduan/{id}/edit', 'PostController@edit');
Route::put('/pengaduan/{id}', 'PostController@update');
Route::delete('/pengaduan/{id}', 'PostController@destroy');

Route::get('/pengaduan/{id}/create-komentar', 'CommentController@createComment');
Route::post('/pengaduan/{id}/create-komentar', 'CommentController@comment')->name('comment');

Route::get('/pengaduan/{id}', 'CommentController@show');

Auth::routes();

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::get('/login/editor', 'Auth\LoginController@showEditorLoginForm');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
Route::get('/register/editor', 'Auth\RegisterController@showEditorRegisterForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/login/editor', 'Auth\LoginController@editorLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
Route::post('/register/editor', 'Auth\RegisterController@createEditor');