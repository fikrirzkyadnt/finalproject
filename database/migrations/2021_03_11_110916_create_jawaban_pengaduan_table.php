<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJawabanPengaduanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawaban_pengaduan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('isi');
            
            $table->unsignedBigInteger('pengaduan_id');
            $table->unsignedBigInteger('petugas_id');

            $table->foreign('petugas_id')->references('id')->on('petugas');
            $table->foreign('pengaduan_id')->references('id')->on('pengaduan');
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawaban_pengaduan');
    }
}
