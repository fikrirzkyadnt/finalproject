<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeKomentarPengaduanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_komentar_pengaduan', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('pengaduan_id');
            $table->unsignedBigInteger('komentar_pengaduan_id');

            $table->foreign('pengaduan_id')->references('id')->on('pengaduan');
            $table->foreign('komentar_pengaduan_id')->references('id')->on('komentar_pengaduan');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_komentar_pengaduan');
    }
}
