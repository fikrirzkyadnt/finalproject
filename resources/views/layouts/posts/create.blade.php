@extends('layouts.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Ajukan Pengaduan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="{{ route('store') }}" method="POST">
            @csrf
            
          <div class="card-body">
            <div class="form-group">
              <label for="judul">Judul Pengaduan</label>
              <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Judul Pengaduan">
              @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="desc">Isi</label>
              <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', '') }}" placeholder="Isi Pengaduan">
              @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="tahun">Kategori Pengaduan</label>
              <input type="text" class="form-control" id="kategori" name="kategori" value="{{ old('kategori', '') }}" placeholder="Kategori Pengaduan">
              @error('kategori')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
</div>

@endsection