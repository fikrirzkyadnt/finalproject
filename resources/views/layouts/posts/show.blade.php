@extends('layouts.master')

@section('content')
    <div class="mt-3 ml-3">

        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body p-4">
                            <div class="row">
                                <div class="col-lg-3">
                                    <img onclick="" style="cursor:pointer" width="240" src="/fotos/no-image.png">
                                </div>
                                <div class="col-lg-8">
                                    <h4>
                                        {{ $show->author->name }}
                                        <small><span
                                                class="bg-success text-white shadow-sm p-1 mb-2 rounded">Terverifikasi</span></small>
                                    </h4>
                                    <small>{{ $show->created_at }}</small>
                                    <p>
                                        {{ $show->judul }}
                                    </p>
                                    <p>
                                        {{ $show->isi }}
                                    </p>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="bg-primary text-white text-center shadow-sm p-1 mb-2 rounded">Instansi
                                            </p>
                                            <div>Kelurahan xxxxxx</div>
                                        </div>
                                        <div class="col-lg-4">
                                            <p class="bg-info text-white text-center shadow-sm p-1 mb-2 rounded">Kategori
                                            </p>
                                            <div>{{ $show->kategori }}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col mt-4">
                                    <a class="btn btn-primary mb-2"
                                        href="/pengaduan/{{ $show->id }}/create-komentar">Berikan Tanggapan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-4 p-4 mb-5">
                        <div class="header">
                            <h4>Tanggapan</h4>
                        </div>
                        <div class"body="" table-responsive"="" style="padding: 10px">
                            <div class="media">
                                <div class="media-left">
                                    <a href="javascript:void(0);">
                                        <img class="media-object"
                                            src="/uploads/avatar/2016/3/6/thumb_pp_sapawarga.jpg?1515720347" width="64"
                                            height="64">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        {{ $show->author->name }}
                                        <small><span
                                                class="bg-success text-white shadow-sm p-1 mb-2 rounded">Terverifikasi</span></small>
                                    </h4>
                                    {{ $show->created_at }}<br>
                                    @forelse ($comments as $key => $comment)
                                        <tr>
                                            {{-- <td> {{ $key + 1 }}</td><br> --}}
                                            <td> {{ $comment->isi }}</td><br>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" align="center">Belum ada komentar</td>
                                        </tr>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
